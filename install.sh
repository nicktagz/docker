#!/bin/bash

installscript() {
[ -d ~/.tagz ] || mkdir ~/.tagz
[ -f ~/docker/install.sh ] && (cp -r ~/docker/.git ~/.tagz/ && cp ~/docker/* ~/.tagz/)
[ -d ~/docker ] && sudo rm -R docker
alias tagz='~/.tagz/install.sh'
[ -d /mnt/downloads ] || (sudo mkdir /mnt/downloads && sudo chown -R "$USER":"$USER" /mnt/downloads)
[ -n "$(find /mnt/downloads -user "$(id -u)" -print -prune -o -prune)" ] || sudo chown -R "$USER":"$USER" /mnt/downloads
[ -d /mnt/tv ] || (sudo mkdir /mnt/tv && sudo chown -R "$USER":"$USER" /mnt/tv)
[ -n "$(find /mnt/tv -user "$(id -u)" -print -prune -o -prune)" ] || sudo chown -R "$USER":"$USER" /mnt/tv
[ -d /mnt/movies ] || (sudo mkdir /mnt/movies && sudo chown -R "$USER":"$USER" /mnt/movies)
[ -n "$(find /mnt/movies -user "$(id -u)" -print -prune -o -prune)" ] || sudo chown -R "$USER":"$USER" /mnt/movies
}

fixpermissions() {
[ -n "$(find /mnt/downloads -user "$(id -u)" -print -prune -o -prune)" ] || sudo chown -R "$USER":"$USER" /mnt/downloads
[ -n "$(find /mnt/tv -user "$(id -u)" -print -prune -o -prune)" ] || sudo chown -R "$USER":"$USER" /mnt/tv
[ -n "$(find /mnt/movies -user "$(id -u)" -print -prune -o -prune)" ] || sudo chown -R "$USER":"$USER" /mnt/movies
}

updatecheck() {
[ -f ~/.tagz/install.sh ] || installscript
cd ~/.tagz || exit
git remote update
UPSTREAM=${1:-'@{u}'}
LOCAL=$(git rev-parse @)
REMOTE=$(git rev-parse "$UPSTREAM")
BASE=$(git merge-base @ "$UPSTREAM")

if [ $LOCAL = $REMOTE ]; then
	echo "Up-to-date"
	requirementcheck
elif [ $LOCAL = $BASE ]; then
    git reset --hard
    git pull
    clear
    tagz
elif [ $REMOTE = $BASE ]; then
    echo "Need to push"
else
    echo "Diverged"
fi
}

requirementcheck() {
[ -f ~/.tagz/install.sh ] || installscript
if [ -x "$(command -v docker)" ]; then
echo "Docker Installed"
else
sudo apt update && sudo apt full-upgrade -y
#sudo nano /etc/apt/apt.conf.d/50unattended-upgrades
sudo apt install curl git unattended-upgrade sqlite3 -y
curl -sSL https://get.docker.com | sh
#sudo usermod -aG docker "$USER"
clear
echo "After reboot type ""$CYAN""tagz""$NC"""
echo ""
read -n 1 -s -r -p "Press any key to reboot"
sudo reboot
fi

sudo docker network ls|grep thedocks > /dev/null || sudo docker network create --attachable thedocks

[ -x "$(command -v unattended-upgrade)" ] || (sudo apt update && sudo apt install unattended-upgrades -y)
[ -x "$(command -v sqlite3)" ] || (sudo apt update && sudo apt install sqlite3 -y)

if sudo docker ps -a --format '{{.Names}}' | grep -Eq "transmission-openvpn" || sudo docker ps -a --format '{{.Names}}' | grep -Eq "sabnzbd"; then
echo "Download Client Installed"
else
clear
echo """$YELLOW""Download Client Selection""$NC"""
echo ""
PS3='
Please enter your choice: '
options=("Transmission" "SABnzbd")
COLUMNS=1
select opt in "${options[@]}"
do
    case $opt in
    "Transmission")
	transmission-openvpninstall
	break
	;;
    "SABnzbd")
    sabnzbdinstall
	break
    ;;
    *)
	echo "Invalid Option"
	;;
   esac
done
fi

clear && watchtowerinstall
#ouroborosinstall
clear && radarrinstall
clear && sonarrinstall
clear && heimdallinstall

MainMenu
}

MainMenu () {
duckdnscheck
if [ "$?" = "1" ];then
dashaddress="$(hostname -I | awk '{print "http://"$1""}')"
else
duckaddress="https://heimdall.""$duckaddress"""
dashaddress="$(hostname -I | awk '{print "http://"$1""":8118"""}')"
fi
clear
echo """$GREEN""****************************************""$NC"""
echo "   		 /\**/\""
echo """		( o_o  )_)"""
echo "     	        ,(u  u  ,),"
echo """$GREEN""""$NC""      ""$BLUEBG""""$BLACK""Tagz Docker Installer Thing""$NC""""$GREEN""     ""$NC"""
echo """$GREEN""****************************************""$NC"""
echo ""
echo "Dashboard ""$BLUE""Internal""$NC"": ""$dashaddress"""
echo ""
echo "Dashboard ""$RED""External""$NC"": ""$duckaddress"""
echo ""
appselect="$YELLOW"'Application Select'"$NC"
install="$GREEN"'Install All'"$NC"
update="$CYAN"'Update Device'"$NC"
appupdate="Update App Settings"
radarrfix="$BLUE"'Fix for Radarr/Plex'"$NC"
quit="$RED"'Quit'"$NC"
if sudo docker ps -a --format '{{.Names}}' | grep -Eq "duckdns" && sudo docker ps -a --format '{{.Names}}' | grep -Eq "swag"; then
external="""$MAGENTA""Current Containers Installed""$NC"""
letsduck="yes"
else
external="""$MAGENTA""Install DuckDNS and SWAG""$NC"""
letsduck="no"
fi
PS3='
Please enter your choice: '
options=("$appselect" "$install" "$update" "$external" "$appupdate" "$radarrfix" "$quit")
COLUMNS=1
select opt in "${options[@]}"
do
    case $opt in
    "$appselect")
	appselect
	break
	;;
    "$install")
    installall
	break
    ;;
	"$update")
	clear
	sudo apt update && sudo apt full-upgrade -y
	MainMenu
	break
	;;
	"$external")
	if [ "$letsduck" = "yes" ];then
	externalcheck
	else
	duckdnsinstall
	swaginstall
	fi
	MainMenu
	break
	;;
	"$appupdate")
	updateapps & progressbar
	MainMenu
	break
	;;
	"$radarrfix")
	clear
	sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 04EE7237B7D453EC 648ACFD622F3D138
    echo "deb http://deb.debian.org/debian buster-backports main" | sudo tee -a /etc/apt/sources.list.d/buster-backports.list
    sudo apt update
    sudo apt install -t buster-backports libseccomp2
	MainMenu
	break
	;;
    "$quit")
	clear
    break
    ;;
    *)
	MainMenu
	break
	;;
   esac
done
}

appselect () {
clear
echo """$BLUE**********************"
echo "* ""$WHITE""Select Application""$BLUE"" *"
echo "**********************""$NC"""
echo ""
installcheck
PS3='
Please enter your choice: '
options=("$couchpotato" "$duckdns" "$heimdall" "$jackett" "$jellyfin" "$ombi" "$plex"
"$portainer" "$radarr" "$sabnzbd" "$sickchill" "$sonarr" "$swag" "$tautulli" "$transmission"
"""$MAGENTA""""Main Menu""$NC""")
COLUMNS=1
select opt in "${options[@]}"
do
    case $opt in
			"$couchpotato")
		installselect couchpotato CouchPotato "https://couchpota.to"
		break
			;;
			"$duckdns")
		installselect duckdns DuckDNS "https://duckdns.org"
		break
	        ;;
			"$heimdall")
      	installselect heimdall Heimdall "https://heimdall.site"
		break
        	;;
			"$jackett")
      	installselect jackett Jackett "https://github.com/Jackett/Jackett"
		break
        	;;
			"$jellyfin")
		installselect jellyfin Jellyfin "https://jellyfin.org"
		break
       	        ;;
			"$ombi")
		installselect ombi Ombi "https://ombi.io"
		break
		;;
			"$plex")
		installselect plex Plex "https://www.plex.tv"
		break
		;;
			"$portainer")
		installselect portainer Portainer "https://www.portainer.io"
		break
		;;
			"$radarr")
		installselect radarr Radarr "https://radarr.video"
		break
		;;
			"$sabnzbd")
		installselect sabnzbd SABnzbd "https://sabnzbd.org"
		break
		;;
			"$sickchill")
		installselect sickchill Sickchill "https://sickchill.github.io"
		break
		;;
			"$sonarr")
		installselect sonarr Sonarr "https://sonarr.tv"
		break
		;;
			"$swag")
		installselect swag SWAG "https://github.com/linuxserver/docker-swag"
		break
		;;
			"$tautulli")
		installselect tautulli Tautulli "https://tautulli.com"
		break
		;;
			"$transmission")
		installselect transmission-openvpn Transmission "https://haugene.github.io/docker-transmission-openvpn/supported-providers"
		break
		;;
			"""$MAGENTA""""Main Menu""$NC""")
        MainMenu
		break
       	;;
        *) echo "invalid option $REPLY";;
    esac
done
}

installcheck() {
ipaddress="$(hostname -I | awk '{print " http://"$1":"}')"
all="$(sudo docker ps -a --format '{{.Names}}' | sort | awk '{print $1}')"
i="${all[*]}"

if [[ "$i" =~ "couchpotato" ]]; then
couchpotato="""$YELLOW""CouchPotato""$NC"" (""$GREEN""installed""$NC"") ""$BLUE""""$ipaddress""""5050""""$NC"""
else
couchpotato="""$YELLOW""CouchPotato""$NC"" (""$RED""not installed""$NC"")"
fi

if [[ "$i" =~ "duckdns" ]]; then
duckdns="""$YELLOW""DuckDNS""$NC"" (""$GREEN""installed""$NC"")"
else
duckdns="""$YELLOW""DuckDNS""$NC"" (""$RED""not installed""$NC"")"
fi

if [[ "$i" =~ "heimdall" ]]; then
port='80'
[[ "$i" =~ "duckdns" ]] && port='8118'
heimdall="""$YELLOW""Heimdall""$NC"" (""$GREEN""installed""$NC"") ""$BLUE""""$ipaddress""""$port""""$NC"""
else
heimdall="""$YELLOW""Heimdall""$NC"" (""$RED""not installed""$NC"")"
fi

if [[ "$i" =~ "jackett" ]]; then
jackett="""$YELLOW""Jackett""$NC"" (""$GREEN""installed""$NC"") ""$BLUE""""$ipaddress""""9117""""$NC"""
else
jackett="""$YELLOW""Jackett""$NC"" (""$RED""not installed""$NC"")"
fi

if [[ "$i" =~ "jellyfin" ]]; then
jellyfin="""$YELLOW""Jellyfin""$NC"" (""$GREEN""installed""$NC"") ""$BLUE""""$ipaddress""""8096""""$NC"""
else
jellyfin="""$YELLOW""Jellyfin""$NC"" (""$RED""not installed""$NC"")"
fi

if [[ "$i" =~ "swag" ]]; then
swag="""$YELLOW""SWAG""$NC"" (""$GREEN""installed""$NC"")"
else
swag="""$YELLOW""SWAG""$NC"" (""$RED""not installed""$NC"")"
fi

if [[ "$i" =~ "ombi" ]]; then
ombi="""$YELLOW""Ombi""$NC"" (""$GREEN""installed""$NC"")  ""$BLUE""""$ipaddress""""3579""""$NC"""
else
ombi="""$YELLOW""Ombi""$NC"" (""$RED""not installed""$NC"")"
fi

if [[ "$i" =~ "plex" ]]; then
plex="""$YELLOW""Plex""$NC"" (""$GREEN""installed""$NC"") ""$BLUE""""$ipaddress""""32400/web""""$NC"""
else
plex="""$YELLOW""Plex""$NC"" (""$RED""not installed""$NC"")"
fi

if [[ "$i" =~ "portainer" ]]; then
portainer="""$YELLOW""Portainer""$NC"" (""$GREEN""installed""$NC"") ""$BLUE""""$ipaddress""""9000""""$NC"""
else
portainer="""$YELLOW""Portainer""$NC"" (""$RED""not installed""$NC"")"
fi

if [[ "$i" =~ "radarr" ]]; then
radarr="""$YELLOW""Radarr""$NC"" (""$GREEN""installed""$NC"") ""$BLUE""""$ipaddress""""7878""""$NC"""
else
radarr="""$YELLOW""Radarr""$NC"" (""$RED""not installed""$NC"")"
fi

if [[ "$i" =~ "sabnzbd" ]]; then
sabnzbd="""$YELLOW""SABnzbd""$NC"" (""$GREEN""installed""$NC"") ""$BLUE""""$ipaddress""""8080""""$NC"""
else
sabnzbd="""$YELLOW""SABnzbd""$NC"" (""$RED""not installed""$NC"")"
fi

if [[ "$i" =~ "sickchill" ]]; then
sickchill="""$YELLOW""Sickchill""$NC"" (""$GREEN""installed""$NC"") ""$BLUE""""$ipaddress""""8081""""$NC"""
else
sickchill="""$YELLOW""Sickchill""$NC"" (""$RED""not installed""$NC"")"
fi

if [[ "$i" =~ "sonarr" ]]; then
sonarr="""$YELLOW""Sonarr""$NC"" (""$GREEN""installed""$NC"") ""$BLUE""""$ipaddress""""8989""""$NC"""
else
sonarr="""$YELLOW""Sonarr""$NC"" (""$RED""not installed""$NC"")"
fi

if [[ "$i" =~ "tautulli" ]]; then
tautulli="""$YELLOW""Tautulli""$NC"" (""$GREEN""installed""$NC"") ""$BLUE""""$ipaddress""""8181""""$NC"""
else
tautulli="""$YELLOW""Tautulli""$NC"" (""$RED""not installed""$NC"")"
fi

if [[ "$i" =~ "transmission-openvpn" ]]; then
transmission="""$YELLOW""Transmission-OpenVPN""$NC"" (""$GREEN""installed""$NC"") ""$BLUE""""$ipaddress""""9091""""$NC"""
else
transmission="""$YELLOW""Transmission-OpenVPN""$NC"" (""$RED""not installed""$NC"")"
fi
}

installselect() {
FILE=~/.config/appdata/swag/nginx/proxy-confs/"$1".subdomain.conf
CONFIGS=~/.config/appdata/"$1"
installmenu "$2" "$3"
case $? in
1*) clear
	"$1""install"
	updateapps
	donemsg "$2"" Installed"
	appselect
	;;
2*) clear
	sudo docker rm -f "$1"
	[ -f "$FILE" ] && (sudo rm "$FILE" && sudo docker container restart swag)
	[ -d "$CONFIGS" ] && sudo rm -R "$CONFIGS"
	updateapps
	sudo docker image prune -af
	donemsg "$2"" Uninstalled"
	appselect
	;;
3*) clear
	sudo docker rm -f "$1"
	[ -d "$CONFIGS" ] && sudo rm -R "$CONFIGS"
	"$1""install"
	updateapps
	donemsg "$2"" Re-Installed"
	appselect	
	;;
4*) "external" "$1"
	sel="$?"
	if [ "$sel" -eq 1 ]; then
	donemsg "$2"" External Access Granted"
	elif [ "$sel" -eq 2 ]; then
	clear
	echo "$YELLOW"
	echo "Install ""$2"" First"
	echo "$NC"
	read -n 1 -s -r -p """$CYAN""Press any key to continue""$NC"""
	else
	clear
	echo "$YELLOW"
	echo "Install SWAG First"
	echo "$NC"
	read -n 1 -s -r -p """$CYAN""Press any key to continue""$NC"""
	fi
	updateapps
	appselect
	;;
5*)	[ -f "$FILE" ] && rm "$FILE"
	updateapps
	sudo docker container restart swag
	donemsg "$2" "External Access Removed"
	;;
6*)	sudo docker rm -f "$1"
	"$1""install"
	updateapps
	donemsg "$2"" Re-Created"
	appselect	
	;;
esac
}

installmenu() {
clear
echo "$MAGENTA""$1""$NC"
echo ""
echo "$2"
echo ""
echo """$YELLOW""******************************""$NC"""
echo ""
install="""$GREEN""Install""$NC"""
uninstall="""$RED""Uninstall""$NC"""
reinstall="""$MAGENTA""Reinstall""$NC"""
recreate="Recreate"
external="""$BLUE""Allow External Access""$NC"""
rmexternal="""$RED""Remove External Access""$NC"""
back="""$CYAN""Back""$NC"""
echo """$YELLOW""Select Option""$NC"""
echo ""
PS3='
Please enter your choice: '
options=("$install" "$uninstall" "$reinstall" "$recreate" "$external" "$rmexternal" "$back")
COLUMNS=1
select opt in "${options[@]}"
do
    case $opt in
	"$install")
        return 1
	break
        ;;
	"$uninstall")
        return 2
	break
        ;;
	"$reinstall")
        return 3
	break
		;;
	"$external")
        return 4
	break
        ;;
	"$rmexternal")
		return 5
	break
		;;
	"$recreate")
		return 6
	break
		;;
	"$back")
	appselect
	break
        ;;
        *) echo "invalid option $REPLY";;
    esac
done
}

external() {
container_name=swag
FILE=~/.config/appdata/swag/nginx/proxy-confs/"$1".subdomain.conf
if sudo docker ps -a --format '{{.Names}}' | grep -Eq "^${container_name}\$"; then
	if sudo docker ps -a --format '{{.Names}}' | grep -Eq "$1"; then
	[ -f "$FILE" ] || cp "$FILE".sample "$FILE"
	sudo docker container restart swag
	return 1
	else
	return 2
	fi
else
return 3
fi
}

externalcheck() {
duckdnscheck
all="$(sudo docker ps -a --format '{{.Names}}' | sort | awk '{print toupper($1)}')"
list2="$(ls ~/.config/appdata/swag/nginx/proxy-confs/*.conf | awk -F '[./]' '{print toupper($10)}')"
list1="${all//TRANSMISSION-OPENVPN/TRANSMISSION}"

i="${list2[*]}"
for item in ${list1[@]}; do
  if [[ $i =~ "$item" ]]; then
    result+=("$YELLOW$item$NC https://${item,,}.$duckaddress")
  else
	result+=("$YELLOW$item$NC")
  fi
done
clear
echo """$MAGENTA""Applications""$NC"""
echo ""
printf "%s\n" "${result[@]}" | column -t
echo ""
read -n 1 -s -r -p """$MAGENTA""Press any key to continue""$NC"""
unset result
}

dockerproject='TAGZ'

autoheal() {
if sudo docker ps -a --format '{{.Names}}' | grep -Eq "autoheal"; then
  echo "Autoheal Installed"
else
sudo docker run -d \
  --name autoheal \
  --restart=always \
  --net=thedocks \
  -e AUTOHEAL_CONTAINER_LABEL=all \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -l com.docker.compose.project="$dockerproject" \
   willfarrell/autoheal
fi
}

couchpotatoinstall() {
if sudo docker ps -a --format '{{.Names}}' | grep -Eq "couchpotato"; then
  echo "CouchPotato Installed"
else
sudo docker run -d \
  --name=couchpotato \
  --net=thedocks \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ=America/New_York \
  -e UMASK_SET=022 \
  -p 5050:5050 \
  -v ~/.config/appdata/couchpotato:/config \
  -v /mnt/downloads:/downloads \
  -v /mnt/movies:/movies \
  -l com.docker.compose.project="$dockerproject" \
  --restart unless-stopped \
  linuxserver/couchpotato
fixpermissions
fi
}

duckdnsinstall() {
if sudo docker ps -a --format '{{.Names}}' | grep -Eq "duckdns"; then
  echo "DuckDNS Installed"
else
clear
echo """$YELLOW""******DuckDNS Install******""$NC"""
echo ""
read -rp "Enter Subdomain (example): " subdomain
read -rp "Enter Token: " token
sudo docker run -d \
--name=duckdns \
--net=thedocks \
-e PUID=1000 \
-e PGID=1000 \
-e TZ=America/New_York \
-e SUBDOMAINS="$subdomain" \
-e TOKEN="$token" \
-e LOG_FILE=false \
-v ~/.config/appdata/duckdns:/config \
-l com.docker.compose.project="$dockerproject" \
--restart unless-stopped \
linuxserver/duckdns
fi
}

heimdallinstall() {
if sudo docker ps -a --format '{{.Names}}' | grep -Eq "heimdall"; then
echo "Heimdall Installed"
else
if sudo docker ps -a --format '{{.Names}}' | grep -Eq "swag"; then
port80="8118"
port443="4443"
FILE=~/.config/appdata/swag/nginx/proxy-confs/heimdall.subdomain.conf
[ -f "$FILE" ] || cp "$FILE".sample "$FILE"
sudo docker container restart swag
else
port80="80"
port443="443"
fi
sudo netstat -pant | grep ":80 " && port80="8118"
sudo netstat -pant | grep ":443 " && port443="4443"
sudo docker run -d \
  --name=heimdall \
  --net=thedocks \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ=America/New_York \
  -p "$port80":80 \
  -p "$port443":443 \
  -v ~/.config/appdata/heimdall:/config \
  -l com.docker.compose.project="$dockerproject" \
  --restart unless-stopped \
  linuxserver/heimdall

while ! sudo docker logs heimdall 2>&1 | grep "services.d" | grep "done";
do
	sleep 1
done & progressbar

hostip="$(hostname -I | awk '{print "http://"$1}')"
hostip2="$(hostname -I | awk '{print $1}')"
radarrapi="$(cat < ~/.config/appdata/radarr/config.xml | grep ApiKey | awk -F ['<>'] '{print $3}')"
sonarrapi="$(cat < ~/.config/appdata/sonarr/config.xml | grep ApiKey | awk -F ['<>'] '{print $3}')"
if [ -z "$(sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "select title from items where title='CouchPotato'")" ]; then
sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "insert into items(title,colour,icon,url,description,pinned,class) values('CouchPotato','#161b1f','icons/couchpotato.png','""$hostip"":5050','{"'"enabled"'":true,"'"override_url"'":null}',0,'\App\SupportedApps\CouchPotato\CouchPotato');" && sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "insert into item_tag(item_id,tag_id) select id,0 from items where title like 'CouchPotato';"
fi
if [ -z "$(sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "select title from items where title='Jackett'")" ]; then
sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "insert into items(title,colour,icon,url,description,pinned,class) values('Jackett','#161b1f','icons/jackett.png','""$hostip"":9117','{"'"enabled"'":true,"'"override_url"'":null}',0,'\App\SupportedApps\Jackett\Jackett');" && sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "insert into item_tag(item_id,tag_id) select id,0 from items where title like 'Jackett';"
fi
if [ -z "$(sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "select title from items where title='Jellyfin'")" ]; then
sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "insert into items(title,colour,icon,url,description,pinned,class) values('Jellyfin','#161b1f','icons/jellyfin.png','""$hostip"":8096','{"'"enabled"'":true,"'"override_url"'":null}',0,'\App\SupportedApps\Jellyfin\Jellyfin');" && sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "insert into item_tag(item_id,tag_id) select id,0 from items where title like 'Jellyfin';"
fi
if [ -z "$(sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "select title from items where title='Ombi'")" ]; then
sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "insert into items(title,colour,icon,url,description,pinned,class) values('Ombi','#161b1f','icons/ombi.png','""$hostip"":3579','{"'"enabled"'":true,"'"override_url"'":null}',1,'\App\SupportedApps\Ombi\Ombi');" && sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "insert into item_tag(item_id,tag_id) select id,0 from items where title like 'Ombi';"
fi
if [ -z "$(sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "select title from items where title='Plex'")" ]; then
sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "insert into items(title,colour,icon,url,description,pinned,class) values('Plex','#161b1f','icons/plex.png','""$hostip"":32400/web','{"'"enabled"'":true,"'"override_url"'":null}',1,'\App\SupportedApps\Plex\Plex');" && sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "insert into item_tag(item_id,tag_id) select id,0 from items where title like 'Plex';"
fi
if [ -z "$(sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "select title from items where title='Portainer'")" ]; then
sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "insert into items(title,colour,icon,url,description,pinned,class) values('Portainer','#161b1f','icons/portainer.png','""$hostip"":9000','{"'"enabled"'":true,"'"override_url"'":null}',1,'\App\SupportedApps\Portainer\Portainer');" && sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "insert into item_tag(item_id,tag_id) select id,0 from items where title like 'Portainer';"
fi
if [ -z "$(sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "select title from items where title='Radarr'")" ]; then
sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "insert into items(title,colour,icon,url,description,pinned,class) values('Radarr','#161b1f','icons/radarr.png','""$hostip"":7878','{"'"enabled"'":true,"'"override_url"'":null,"'"apikey"'":\"""$radarrapi""\"}',1,'\App\SupportedApps\Radarr\Radarr');" && sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "insert into item_tag(item_id,tag_id) select id,0 from items where title like 'Radarr';"
fi
if [ -z "$(sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "select title from items where title='SABnzbd'")" ]; then
sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "insert into items(title,colour,icon,url,description,pinned,class) values('SABnzbd','#161b1f','icons/sabnzbd.png','""$hostip"":8080','{"'"enabled"'":true,"'"override_url"'":null,"'"apikey"'":null}',0,'\App\SupportedApps\SABnzbd\SABnzbd');" && sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "insert into item_tag(item_id,tag_id) select id,0 from items where title like 'SABnzbd';"
fi
if [ -z "$(sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "select title from items where title='Sickchill'")" ]; then
sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "insert into items(title,colour,icon,url,description,pinned,class) values('Sickchill','#161b1f','icons/sickchill.png','""$hostip"":8081','{"'"enabled"'":true,"'"override_url"'":null}',0,'\App\SupportedApps\Sickchill\Sickchill');" && sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "insert into item_tag(item_id,tag_id) select id,0 from items where title like 'Sickchill';"
fi
if [ -z "$(sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "select title from items where title='Sonarr'")" ]; then
sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "insert into items(title,colour,icon,url,description,pinned,class) values('Sonarr','#161b1f','icons/sonarr.png','""$hostip"":8989','{"'"enabled"'":true,"'"override_url"'":null,"'"apikey"'":\"""$sonarrapi""\"}',1,'\App\SupportedApps\Sonarr\Sonarr');" && sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "insert into item_tag(item_id,tag_id) select id,0 from items where title like 'Sonarr';"
fi
if [ -z "$(sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "select title from items where title='Tautulli'")" ]; then
sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "insert into items(title,colour,icon,url,description,pinned,class) values('Tautulli','#161b1f','icons/tautulli.png','""$hostip"":8181','{"'"enabled"'":true,"'"override_url"'":null}',0,'\App\SupportedApps\Tautulli\Tautulli');" && sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "insert into item_tag(item_id,tag_id) select id,0 from items where title like 'Tautulli';"
fi
if [ -z "$(sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "select title from items where title='Transmission'")" ]; then
sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "insert into items(title,colour,icon,url,description,pinned,class) values('Transmission','#161b1f','icons/transmission.png','""$hostip"":9091','{"'"enabled"'":true,"'"override_url"'":null,"'"username"'":\"""$transuser""\","'"password"'":\"""$transpass""\"}',0,'\App\SupportedApps\Transmission\Transmission');" && sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "insert into item_tag(item_id,tag_id) select id,0 from items where title like 'Transmission';"
fi
[ -d ~/.config/appdata/heimdall/www/backgrounds ] || sudo mkdir ~/.config/appdata/heimdall/www/backgrounds
[ -f ~/.tagz/background.jpg ] && sudo cp ~/.tagz/background.jpg ~/.config/appdata/heimdall/www/backgrounds/
if [ -z "$(sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "select setting_id from setting_user")" ]; then
sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "insert into setting_user(setting_id,user_id,uservalue) values(2,1,'backgrounds/background.jpg');"
fi
updateapps & progressbar
sudo docker container restart heimdall
fi
}

jackettinstall() {
if sudo docker ps -a --format '{{.Names}}' | grep -Eq "jackett"; then
  echo "Jackett Installed"
else
sudo docker run -d \
--name=jackett \
--net=thedocks \
-e PUID=1000 \
-e PGID=1000 \
-e TZ=America/New_York \
-p 9117:9117 \
-v ~/.config/appdata/jackett:/config \
-l com.docker.compose.project="$dockerproject" \
--restart unless-stopped \
linuxserver/jackett
fi
}

jellyfininstall() {
if sudo docker ps -a --format '{{.Names}}' | grep -Eq "jellyfin"; then
  echo "Jellyfin Installed"
else
sudo docker run -d \
--name=jellyfin \
--net=thedocks \
-e PUID=1000 \
-e PGID=1000 \
-e TZ=America/New_York \
-p 8096:8096 \
-p 8920:8920 \
-v ~/.config/appdata/jellyfin:/config \
-v /mnt/movies:/movies \
-v /mnt/tv:/tv \
-l com.docker.compose.project="$dockerproject" \
--restart unless-stopped \
linuxserver/jellyfin
fi
}

ombiinstall() {
sudo docker run -d \
  --name=ombi \
  --net=thedocks \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ=America/New_York \
  -p 3579:3579 \
  -v ~/.config/appdata/ombi:/config \
  -l com.docker.compose.project="$dockerproject" \
  --restart unless-stopped \
  linuxserver/ombi
  ombisettings
}

ouroborosinstall() {
if sudo docker ps -a --format '{{.Names}}' | grep -Eq "ouroboros"; then
echo "Ouroboros Installed"
else
sudo docker run -d \
--name=ouroboros \
--net=thedocks \
-e TZ=America/New_York \
-e CLEANUP=true \
-e CRON=0\ 4\ *\ *\ * \
-e INTERVAL=300 \
-e LOG_LEVEL=info \
-e SELF_UPDATE=true \
-e RUN_ONCE=false \
-e LATEST=true \
-v /var/run/docker.sock:/var/run/docker.sock \
-l com.docker.compose.project="$dockerproject" \
--restart unless-stopped \
pyouroboros/ouroboros
fi
}

plexinstall() {
if sudo docker ps -a --format '{{.Names}}' | grep -Eq "plex"; then
  echo "Plex Installed"
else
sudo docker run -d \
--name=plex \
--net=host \
-e PUID=1000 \
-e PGID=1000 \
-e VERSION=docker \
-v ~/.config/appdata/plex:/config \
-v /mnt/movies:/movies \
-v /mnt/tv:/tv \
-l com.docker.compose.project="$dockerproject" \
--restart unless-stopped \
linuxserver/plex
fi
}

portainerinstall() {
if sudo docker ps -a --format '{{.Names}}' | grep -Eq "portainer"; then
echo "Portainer Installed"
else
sudo docker run -d \
--name=portainer \
--net=thedocks \
-p 9000:9000 \
-p 8000:8000 \
-v /var/run/docker.sock:/var/run/docker.sock \
-v ~/.config/appdata/portainer:/data \
-l com.docker.compose.project="$dockerproject" \
--restart always \
portainer/portainer-ce
fi
}

radarrinstall() {
if sudo docker ps -a --format '{{.Names}}' | grep -Eq "radarr"; then
  echo "Radarr Installed"
else
sudo docker run -d \
--name=radarr \
--net=thedocks \
-e PUID=1000 \
-e PGID=1000 \
-e TZ=America/New_York \
-p 7878:7878 \
-v ~/.config/appdata/radarr:/config \
-v /mnt/movies:/movies \
-v /mnt/downloads:/downloads \
-l com.docker.compose.project="$dockerproject" \
--restart unless-stopped \
linuxserver/radarr
fixpermissions
radarrsettings
fi
}

sabnzbdinstall() {
if sudo docker ps -a --format '{{.Names}}' | grep -Eq "sabnzbd"; then
  echo "SABnzbd Installed"
else
  sudo docker run -d \
  --name=sabnzbd \
  --net=thedocks \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ=America/New_York \
  -p 8080:8080 \
  -p 9090:9090 \
  -v ~/.config/appdata/sabnzbd:/config \
  -v /mnt/downloads:/downloads \
  -l com.docker.compose.project="$dockerproject" \
  --restart unless-stopped \
  linuxserver/sabnzbd
sabnzbdsettings
fixpermissions
fi
}

sickchillinstall() {
if sudo docker ps -a --format '{{.Names}}' | grep -Eq "sickchill"; then
  echo "Sickchill Installed"
else
sudo docker run -d \
  --name=sickchill \
  --net=thedocks \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ=America/New_York \
  -p 8081:8081 \
  -v ~/.config/appdata/sickchill:/config \
  -v /mnt/downloads:/downloads \
  -v /mnt/tv:/tv \
  -l com.docker.compose.project="$dockerproject" \
  --restart unless-stopped \
  linuxserver/sickchill
fixpermissions
fi
}

sonarrinstall() {
if sudo docker ps -a --format '{{.Names}}' | grep -Eq "sonarr"; then
  echo "Sonarr Installed"
else
sudo docker run -d \
--name=sonarr \
--net=thedocks \
-e PUID=1000 \
-e PGID=1000 \
-e TZ=America/New_York \
-p 8989:8989 \
-v ~/.config/appdata/sonarr:/config \
-v /mnt/tv:/tv \
-v /mnt/downloads:/downloads \
-l com.docker.compose.project="$dockerproject" \
--restart unless-stopped \
linuxserver/sonarr
fixpermissions
sonarrsettings
fi
}

swaginstall() {
if sudo docker ps -a --format '{{.Names}}' | grep -Eq "swag"; then
  echo "SWAG Installed"
else
clear
echo """$YELLOW""******SWAG Install******""$NC"""
echo ""
read -rp "Enter URL (""$CYAN""example.duckdns.org""$NC""): " url 
read -rp "Enter DuckDNS Token: " token
sudo docker run -d \
--name=swag \
--net=thedocks \
--cap-add=NET_ADMIN \
-e PUID=1000 \
-e PGID=1000 \
-e TZ=America/New_York \
-e URL="$url" \
-e SUBDOMAINS=wildcard \
-e VALIDATION=duckdns \
-e DUCKDNSTOKEN="$token" \
-e DHLEVEL=2048 \
-e ONLY_SUBDOMAINS=false \
-p 443:443 \
-p 80:80 \
-v ~/.config/appdata/swag:/config \
-l com.docker.compose.project="$dockerproject" \
--restart unless-stopped \
linuxserver/swag

timeout 90 bash -c -- 'while ! sudo docker logs swag 2>&1 | grep 'Server ready';
do
	sleep 1
done' & progressbar
updateapps
fi
}

tautulliinstall() {
if sudo docker ps -a --format '{{.Names}}' | grep -Eq "tautulli"; then
  echo "Tautulli Installed"
else
sudo docker run -d \
--name=tautulli \
--net=thedocks \
-e PUID=1000 \
-e PGID=1000 \
-e TZ=America/New_York \
-p 8181:8181 \
-v ~/.config/appdata/tautulli:/config \
-v ~/.config/appdata/tautulli:/logs \
-l com.docker.compose.project="$dockerproject" \
--restart unless-stopped \
linuxserver/tautulli
tautullisettings
fi
}

transmission-openvpninstall() {
if sudo docker ps -a --format '{{.Names}}' | grep -Eq "transmission-openvpn"; then
  echo "Transmission Installed"
else
clear
echo """$YELLOW""******Transmission Install******""$NC"""
echo ""
echo "All providers:  https://github.com/haugene/docker-transmission-openvpn/tree/master/openvpn"
echo ""
read -rp "Enter ${RED}VPN${NC} Provider (""$CYAN""FASTESTVPN, SURFSHARK, CUSTOM""$NC""): " provider
read -rp "Enter ${RED}VPN${NC} Username: " ovpnuser
read -rp "Enter ${RED}VPN${NC} Password: " ovpnpassword
if [[ ${provider,,} = fastestvpn ]] ; then
ovpnconfig="US-New.York-UDP"
ovpnopts="--auth-nocache --mute-replay-warnings"
elif [[ ${provider,,} = surfshark ]] ; then
ovpnconfig="us-nyc_udp"
ovpnopts="--mute-replay-warnings"
fi
localnetwork="$(hostname -I | awk '{print $1}' | cut -d'.' -f 1-3 | awk '{print $1".0/24"}')"
echo -e -n "Enter a Username for ${GREEN}Transmission${NC}: "
read -r transuser
echo -e -n "Enter a Password for ${GREEN}Transmission${NC}: "
read -r transpassword
#os="$(uname -m | cut -c-3  | awk '{print $1}')"
sudo docker run -d \
--name=transmission-openvpn \
--net=thedocks \
--cap-add=NET_ADMIN -d \
-v /mnt/downloads/:/data \
-v /mnt/downloads/:/downloads \
-v ~/.config/appdata/.openvpn/:/config/openvpn \
-v ~/.config/appdata/transmissionvpn/:/config \
-e OPENVPN_PROVIDER="${provider^^}" \
-e OPENVPN_USERNAME="$ovpnuser" \
-e OPENVPN_PASSWORD="$ovpnpassword" \
-e OPENVPN_CONFIG="$ovpnconfig" \
-e OPENVPN_OPTS="$ovpnopts" \
-e TZ=America/New_York \
-e PGID=1000 \
-e PUID=1000 \
-e LOCAL_NETWORK="$localnetwork" \
-e TRANSMISSION_DOWNLOAD_DIR=/downloads \
-e TRANSMISSION_INCOMPLETE_DIR=/downloads \
-e TRANSMISSION_RATIO_LIMIT=0 \
-e TRANSMISSION_RATIO_LIMIT_ENABLED=true \
-e TRANSMISSION_RPC_AUTHENTICATION_REQUIRED=true \
-e TRANSMISSION_RPC_PASSWORD="$transpassword" \
-e TRANSMISSION_RPC_USERNAME="$transuser" \
-e TRANSMISSION_WATCH_DIR_ENABLED=false \
-e TRANSMISSION_HOME=/config \
--log-driver json-file \
--log-opt max-size=10m \
-p 9091:9091 \
-l com.docker.compose.project="$dockerproject" \
--restart unless-stopped \
haugene/transmission-openvpn
transmissionsettings
fixpermissions
fi
autohealinstall
}

watchtowerinstall() {
if sudo docker ps -a --format '{{.Names}}' | grep -Eq "watchtower"; then
  echo "Watchtower Installed"
else
sudo docker run -d \
--name=watchtower \
--net=thedocks \
-e TZ=America/New_York \
-e WATCHTOWER_CLEANUP=true \
-e WATCHTOWER_INCLUDE_STOPPED=true \
-e WATCHTOWER_SCHEDULE=0\ 0\ 4\ *\ *\ * \
-e WATCHTOWER_DEBUG=false \
-v /var/run/docker.sock:/var/run/docker.sock \
-l com.docker.compose.project="$dockerproject" \
--restart unless-stopped \
containrrr/watchtower
fi
}

installall() {
clear
read -rp """$YELLOW""Are you sure you want to install everything? (""$GREEN""y""$NC""/""$RED""n""$NC"")" -n 1
if [ "${REPLY,,}" == "y" ]; then
couchpotatoinstall
duckdnsinstall
heimdallinstall
jackettinstall
jellyfininstall
swaginstall
ombiinstall
plexinstall
portainerinstall
radarrinstall
sabnzbdinstall
sickchillinstall
sonarrinstall
tautulliinstall
transmission-openvpninstall
updateapps
clear
echo "$GREEN*************$NC"
echo "    ""$RED""D""$GREEN""o""$BLUE""n""$YELLOW""e""$RED""!    $NC"
echo "$GREEN*************$NC"
echo ""
read -rp """$BLUE""Go to Applications Menu? (""$GREEN""y""$NC""/""$RED""n""$NC"")" -n 1
if [ "${REPLY,,}" == "y" ]; then
appselect
else
clear
fi
else
clear
MainMenu
fi
}

donemsg() {
clear
echo "$GREEN*************$NC"
echo "    ""$RED""D""$GREEN""o""$BLUE""n""$YELLOW""e""$RED""!    $NC"
echo "$GREEN*************$NC"
echo ""
echo """$YELLOW""""$1""""$2"""
echo ""
read -n 1 -s -r -p """$CYAN""Press any key to continue""$NC"""
}

duckdnscheck() {
if sudo docker ps -a --format '{{.Names}}' | grep -Eq "duckdns"; then
duckaddress="$(sudo docker container inspect duckdns | grep "SUBDOMAINS" | awk -F '[=",]' '{print $3".duckdns.org"}')"
else
duckaddress="""$MAGENTA""Install DuckDNS and SWAG""$NC"" https://duckdns.org"
return 1
fi
}

ombisettings() {
timeout 30 bash -c -- 'while ! sudo docker logs ombi 2>&1 | grep "Application started";
do
	sleep 1
done' & progressbar
if sudo docker ps -a --format '{{.Names}}' | grep -Eq "heimdall"; then
ombiapi="$(sqlite3 ~/.config/appdata/ombi/OmbiSettings.db "select * from GlobalSettings where SettingsName='OmbiSettings';" | grep ApiKey | awk -F [,:'"'] '{print $17}')"
sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "update items set description='{"'"enabled"'":true,"'"override_url"'":null,"'"apikey"'":\"""$ombiapi""\"}' where title='Ombi';"
fi
}

radarrsettings() {
while ! sudo docker logs radarr 2>&1 | grep Info | grep CommandExecutor | grep Starting;
do
	sleep 1
done & progressbar

until $(curl --output /dev/null --silent --head --fail http://"$(hostname -I | awk '{print $1}')":7878);
do
	sleep 1
done & progressbar

hostip="$(hostname -I | awk '{print $1}')"
if sudo docker ps -a --format '{{.Names}}' | grep -Eq "transmission-openvpn"; then
transmissionstuff
if [ -z "$(sqlite3 ~/.config/appdata/radarr/radarr.db "select Name from DownloadClients where Name='Transmission'")" ]; then
sqlite3 ~/.config/appdata/radarr/radarr.db "insert into DownloadClients(Enable,Name,Implementation,Settings,ConfigContract) values(1,'Transmission','Transmission','{"'"host"'": \"""$hostip""\","'"port"'": 9091,"'"urlBase"'": "'"/transmission/"'","'"username"'": \"""$transuser""\","'"password"'": \"""$transpass""\"}','TransmissionSettings');"
fi
elif sudo docker ps -a --format '{{.Names}}' | grep -Eq "sabnzbd"; then
sabnzbdstuff
if [ -z "$(sqlite3 ~/.config/appdata/radarr/radarr.db "select Name from DownloadClients where Name='SABnzbd'")" ]; then
sqlite3 ~/.config/appdata/radarr/radarr.db "insert into DownloadClients(Enable,Name,Implementation,Settings,ConfigContract) values(1,'SABnzbd','Sabnzbd','{"'"host"'": \"""$hostip""\","'"port"'": 8080,"'"apiKey"'": \"""$sabnzbdapi""\"""$sabnzbduser""""$sabnzbdpass"","'"movieCategory"'": "'"movies"'","'"recentMoviePriority"'": -100,"'"olderMoviePriority"'": -100,"'"useSsl"'": ""$sabnzbdssl""}','SabnzbdSettings');"
fi
fi
if [ -z "$(sqlite3 ~/.config/appdata/radarr/radarr.db "select Key from Config where Key='removecompleteddownloads'")" ]; then
sqlite3 ~/.config/appdata/radarr/radarr.db "insert into Config(Key,Value) values('removecompleteddownloads','True');"
fi
if [ -z "$(sqlite3 ~/.config/appdata/radarr/radarr.db "select Key from Config where Key='filedate'")" ]; then
sqlite3 ~/.config/appdata/radarr/radarr.db "insert into Config(Key,Value) values('filedate','Release');"
fi
if [ -z "$(sqlite3 ~/.config/appdata/radarr/radarr.db "select Key from Config where Key='autorenamefolders'")" ]; then
sqlite3 ~/.config/appdata/radarr/radarr.db "insert into Config(Key,Value) values('autorenamefolders','True');"
fi
if [ -z "$(sqlite3 ~/.config/appdata/radarr/radarr.db "select Key from Config where Key='pathsdefaultstatic'")" ]; then
sqlite3 ~/.config/appdata/radarr/radarr.db "insert into Config(Key,Value) values('pathsdefaultstatic','False');"
fi
if [ -z "$(sqlite3 ~/.config/appdata/radarr/radarr.db "select Name from Indexers where Name='Rarbg'")" ]; then
sqlite3 ~/.config/appdata/radarr/radarr.db "insert into Indexers(Name,Implementation,Settings,ConfigContract,EnableRss,EnableAutomaticSearch,EnableInteractiveSearch) values('Rarbg','Rarbg','{"'"baseUrl"'": "'"https://torrentapi.org"'","'"rankedOnly"'": false,"'"multiLanguages"'": [],"'"minimumSeeders"'": 1,"'"requiredFlags"'": [],"'"categories"'": [14,48,17,44,45,47,50,51,52,42,46]}','RarbgSettings',1,1,1);"
fi
if [ -z "$(sqlite3 ~/.config/appdata/radarr/radarr.db "select Path from RootFolders where Path='/movies/'")" ]; then
sqlite3 ~/.config/appdata/radarr/radarr.db "insert into RootFolders(Path) values('/movies/');"
fi
if [ -z "$(sqlite3 ~/.config/appdata/radarr/radarr.db "select RenameMovies from NamingConfig where RenameMovies='1'")" ]; then
sqlite3 ~/.config/appdata/radarr/radarr.db "insert into NamingConfig(MultiEpisodeStyle,ReplaceIllegalCharacters,StandardMovieFormat,MovieFolderFormat,ColonReplacementFormat,RenameMovies) values(0,1,'{Movie Title} ({Release Year})','{Movie Title} ({Release Year})',0,1)"
fi
if sudo docker ps -a --format '{{.Names}}' | grep -Eq "heimdall"; then
radarrapi="$(cat < ~/.config/appdata/radarr/config.xml | grep ApiKey | awk -F ['<>'] '{print $3}')"
sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "update items set description='{"'"enabled"'":true,"'"override_url"'":null,"'"apikey"'":\"""$radarrapi""\"}' where title='Radarr';"
fi
sudo docker container restart radarr
}

sabnzbdsettings() {
while ! sudo docker logs sabnzbd 2>&1 | grep "Sending notification" | grep "started";
do
	sleep 1
done & progressbar

if sudo docker ps -a --format '{{.Names}}' | grep -Eq "heimdall"; then
sabnzbdapi="$(cat < ~/.config/appdata/sabnzbd/sabnzbd.ini | grep api_key | awk NR==1'{print $3}')"
sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "update items set description='{"'"enabled"'":true,"'"override_url"'":null,"'"apikey"'":\"""$sabnzbdapi""\"}' where title='SABnzbd';"
fi
}

sonarrsettings() {
while ! sudo docker logs sonarr 2>&1 | grep Info | grep "Starting Web Server";
do
	sleep 1
done & progressbar

until $(curl --output /dev/null --silent --head --fail http://"$(hostname -I | awk '{print $1}')":8989);
do
	sleep 1
done & progressbar

hostip="$(hostname -I | awk '{print $1}')"
if sudo docker ps -a --format '{{.Names}}' | grep -Eq "transmission-openvpn"; then
transmissionstuff
if [ -z "$(sqlite3 ~/.config/appdata/sonarr/sonarr.db "select Name from DownloadClients where Name='Transmission'")" ]; then
sqlite3 ~/.config/appdata/sonarr/sonarr.db "insert into DownloadClients(Enable,Name,Implementation,Settings,ConfigContract) values(1,'Transmission','Transmission','{"'"host"'": \"""$hostip""\","'"port"'": 9091,"'"urlBase"'": "'"/transmission/"'","'"username"'": \"""$transuser""\","'"password"'": \"""$transpass""\"}','TransmissionSettings');"
fi
elif sudo docker ps -a --format '{{.Names}}' | grep -Eq "sabnzbd"; then
sabnzbdstuff
if [ -z "$(sqlite3 ~/.config/appdata/sonarr/sonarr.db "select Name from DownloadClients where Name='SABnzbd'")" ]; then
sqlite3 ~/.config/appdata/sonarr/sonarr.db "insert into DownloadClients(Enable,Name,Implementation,Settings,ConfigContract) values(1,'SABnzbd','Sabnzbd','{"'"host"'": \"""$hostip""\","'"port"'": 8080,"'"apiKey"'": \"""$sabnzbdapi""\"""$sabnzbduser""""$sabnzbdpass"","'"tvCategory"'": "'"tv"'","'"recentTvPriority"'": -100,"'"olderTvPriority"'": -100,"'"useSsl"'": ""$sabnzbdssl""}','SabnzbdSettings');"
fi
fi
if [ -z "$(sqlite3 ~/.config/appdata/sonarr/sonarr.db "select Key from Config where Key='removecompleteddownloads'")" ]; then
sqlite3 ~/.config/appdata/sonarr/sonarr.db "insert into Config(Key,Value) values('removecompleteddownloads','True');"
fi
if [ -z "$(sqlite3 ~/.config/appdata/sonarr/sonarr.db "select Key from Config where Key='filedate'")" ]; then
sqlite3 ~/.config/appdata/sonarr/sonarr.db "insert into Config(Key,Value) values('filedate','LocalAirDate');"
fi
if [ -z "$(sqlite3 ~/.config/appdata/sonarr/sonarr.db "select Name from Indexers where Name='Rarbg'")" ]; then
sqlite3 ~/.config/appdata/sonarr/sonarr.db "insert into Indexers(Name,Implementation,Settings,ConfigContract,EnableRss,EnableAutomaticSearch,EnableInteractiveSearch) values('Rarbg','Rarbg','{"'"baseUrl"'": "'"https://torrentapi.org"'","'"rankedOnly"'": false,"'"minimumSeeders"'": 1,"'"seedCriteria"'": {}}','RarbgSettings',1,1,1);"
fi
if [ -z "$(sqlite3 ~/.config/appdata/sonarr/sonarr.db "select Path from RootFolders where Path='/tv/'")" ]; then
sqlite3 ~/.config/appdata/sonarr/sonarr.db "insert into RootFolders(Path) values('/tv/');"
fi
if [ -z "$(sqlite3 ~/.config/appdata/sonarr/sonarr.db "select RenameEpisodes from NamingConfig where RenameEpisodes='1'")" ]; then
sqlite3 ~/.config/appdata/sonarr/sonarr.db "insert into NamingConfig(MultiEpisodeStyle,RenameEpisodes,StandardEpisodeFormat,DailyEpisodeFormat,SeasonFolderFormat,SeriesFolderFormat,AnimeEpisodeFormat) values(0,1,'{Series Title} - S{season:00}E{episode:00} - {Episode Title}','{Series Title} - {Air-Date} - {Episode Title}','Season {season}','{Series Title}','{Series Title} - S{season:00}E{episode:00} - {Episode Title}');"
fi
if sudo docker ps -a --format '{{.Names}}' | grep -Eq "heimdall"; then
sonarrapi="$(cat < ~/.config/appdata/sonarr/config.xml | grep ApiKey | awk -F ['<>'] '{print $3}')"
sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "update items set description='{"'"enabled"'":true,"'"override_url"'":null,"'"apikey"'":\"""$sonarrapi""\"}' where title='Sonarr';"
fi
sudo docker container restart sonarr
}

tautullisettings() {
timeout 30 bash -c -- 'while ! sudo docker logs tautulli 2>&1 | grep "Tautulli is ready";
do
	sleep 1
done' & progressbar
if sudo docker ps -a --format '{{.Names}}' | grep -Eq "heimdall"; then
tautapi="$(cat < ~/.config/appdata/tautulli/config.ini | grep api_key | awk NR==1'{print $3}')"
sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "update items set description='{"'"enabled"'":true,"'"override_url"'":null,"'"apikey"'":\"""$tautapi""\"}' where title='Tautulli';"
fi
}

transmissionsettings() {
timeout 30 bash -c -- 'while ! sudo docker logs transmission-openvpn 2>&1 | grep "Initialization Sequence Completed";
do
	sleep 1
done' & progressbar
if sudo docker ps -a --format '{{.Names}}' | grep -Eq "heimdall"; then
transmissionstuff
sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "update items set description='{"'"enabled"'":true,"'"override_url"'":null,"'"username"'":\"""$transuser""\","'"password"'":\"""$transpass""\"}' where title='Transmission';"
fi
}

progressbar() {
PID=$!
clear
echo "THIS MAY TAKE A WHILE, PLEASE BE PATIENT..."
printf "["
# While process is running...
while kill -0 $PID 2> /dev/null; do 
    printf  "▓"
	sleep 1
done
echo "] done!"
echo ""
}

updateapps() {
hostip="$(hostname -I | awk '{print $1}')"

if sudo docker ps -a --format '{{.Names}}' | grep -Eq "heimdall"; then
all="$(sudo docker ps -a --format '{{.Names}}' | sort | awk '{print $1}')"
list1="${all//transmission-openvpn/transmission}"
all2=(CouchPotato Jackett Jellyfin Ombi Plex Portainer Radarr SABnzbd Sickchill Sonarr Tautulli Transmission)

i="${list1[*]}"
for item in ${all2[@]}; do
if [[ ${i} =~ "${item,,}" ]]; then
sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "update items set pinned='1' where title = '""$item""';"
else
sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "update items set pinned='0' where title = '""$item""';"
fi
done

if sudo docker ps -a --format '{{.Names}}' | grep -Eq "duckdns"; then
if sudo docker ps -a --format '{{.Names}}' | grep -Eq "swag"; then
[ "$(sudo docker port heimdall | awk -F ['/:'] NR==2'{print $3}')" = "80" ] && (sudo docker rm -f heimdall && heimdallinstall)
list2="$(ls ~/.config/appdata/swag/nginx/proxy-confs/*.conf | awk -F '[./]' '{print $10}')"
duckaddress="$(sudo docker container inspect duckdns | grep "SUBDOMAINS" | awk -F '[=",]' '{print $3".duckdns.org"}')"
i="${list2[*]}"
for item in ${all2[@]}; do
if [[ $i =~ ${item,,} ]]; then
sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "update items set url='""https://""${item,,}"""."""$duckaddress""' where title = '""$item""';"
fi
done
fi
fi

if sudo docker ps -a --format '{{.Names}}' | grep -Eq "ombi"; then
ombiapi="$(sqlite3 ~/.config/appdata/ombi/OmbiSettings.db "select * from GlobalSettings where SettingsName='OmbiSettings';" | grep ApiKey | awk -F [,:'"'] '{print $17}')"
sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "update items set description='{"'"enabled"'":true,"'"override_url"'":null,"'"apikey"'":\"""$ombiapi""\"}' where title='Ombi';"
fi

if sudo docker ps -a --format '{{.Names}}' | grep -Eq "radarr"; then
radarrapi="$(cat < ~/.config/appdata/radarr/config.xml | grep ApiKey | awk -F ['<>'] '{print $3}')"
sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "update items set description='{"'"enabled"'":true,"'"override_url"'":null,"'"apikey"'":\"""$radarrapi""\"}' where title='Radarr';"
if sudo docker ps -a --format '{{.Names}}' | grep -Eq "transmission-openvpn"; then
transmissionstuff
sqlite3 ~/.config/appdata/radarr/radarr.db "update DownloadClients set Settings='{"'"host"'": \"""$hostip""\","'"port"'": 9091,"'"urlBase"'": "'"/transmission/"'","'"username"'": \"""$transuser""\","'"password"'": \"""$transpass""\"}' where Name='Transmission';"
elif sudo docker ps -a --format '{{.Names}}' | grep -Eq "sabnzbd"; then
sabnzbdstuff
sqlite3 ~/.config/appdata/radarr/radarr.db "update DownloadClients set Settings='{"'"host"'": \"""$hostip""\","'"port"'": 8080,"'"apiKey"'": \"""$sabnzbdapi""\"""$sabnzbduser""""$sabnzbdpass"","'"movieCategory"'": "'"movies"'","'"recentMoviePriority"'": -100,"'"olderMoviePriority"'": -100,"'"useSsl"'": ""$sabnzbdssl""}' where Name='SABnzbd';"
fi
fi

if sudo docker ps -a --format '{{.Names}}' | grep -Eq "sabnzbd"; then
sabnzbdapi="$(cat < ~/.config/appdata/sabnzbd/sabnzbd.ini | grep api_key | awk NR==1'{print $3}')"
sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "update items set description='{"'"enabled"'":true,"'"override_url"'":null,"'"apikey"'":\"""$sabnzbdapi""\"}' where title='SABnzbd';"
fi

if sudo docker ps -a --format '{{.Names}}' | grep -Eq "sonarr"; then
sonarrapi="$(cat < ~/.config/appdata/sonarr/config.xml | grep ApiKey | awk -F ['<>'] '{print $3}')"
sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "update items set description='{"'"enabled"'":true,"'"override_url"'":null,"'"apikey"'":\"""$sonarrapi""\"}' where title='Sonarr';"
if sudo docker ps -a --format '{{.Names}}' | grep -Eq "transmission-openvpn"; then
transmissionstuff
sqlite3 ~/.config/appdata/sonarr/sonarr.db "update DownloadClients set Settings='{"'"host"'": \"""$hostip""\","'"port"'": 9091,"'"urlBase"'": "'"/transmission/"'","'"username"'": \"""$transuser""\","'"password"'": \"""$transpass""\"}' where Name='Transmission';"
elif sudo docker ps -a --format '{{.Names}}' | grep -Eq "sabnzbd"; then
sabnzbdstuff
sqlite3 ~/.config/appdata/sonarr/sonarr.db "update DownloadClients set Settings='{"'"host"'": \"""$hostip""\","'"port"'": 8080,"'"apiKey"'": \"""$sabnzbdapi""\"""$sabnzbduser""""$sabnzbdpass"","'"tvCategory"'": "'"tv"'","'"recentTvPriority"'": -100,"'"olderTvPriority"'": -100,"'"useSsl"'": ""$sabnzbdssl""}' where Name='SABnzbd';"
fi
fi

if sudo docker ps -a --format '{{.Names}}' | grep -Eq "tautulli"; then
tautapi="$(cat < ~/.config/appdata/tautulli/config.ini | grep api_key | awk NR==1'{print $3}')"
sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "update items set description='{"'"enabled"'":true,"'"override_url"'":null,"'"apikey"'":\"""$tautapi""\"}' where title='Tautulli';"
fi

if sudo docker ps -a --format '{{.Names}}' | grep -Eq "transmission-openvpn"; then
transmissionstuff
sqlite3 ~/.config/appdata/heimdall/www/app.sqlite "update items set description='{"'"enabled"'":true,"'"override_url"'":null,"'"username"'":\"""$transuser""\","'"password"'":\"""$transpass""\"}' where title='Transmission';"
fi

fi
}

sabnzbdstuff() {
sabnzbdapi="$(cat < ~/.config/appdata/sabnzbd/sabnzbd.ini | grep api_key | awk NR==1'{print $3}')"
sabnzbdssl="$(cat < ~/.config/appdata/sabnzbd/sabnzbd.ini | grep 'enable_https' | awk -F [=' '] NR==2'{print $4}')"
[ "$sabnzbdssl" = "0" ] && sabnzbdssl=false || sabnzbdssl=true
sabnzbduser="$(cat < ~/.config/appdata/sabnzbd/sabnzbd.ini | grep 'username' | awk -F [=' '] NR==1'{print "username " $4}' | awk -v q='"' '$1=","q$1q":",$2=q$2q')"
[ "$sabnzbduser" = ","'"username"'"":" "'""""'"" ] && sabnzbduser=
sabnzbdpass="$(cat < ~/.config/appdata/sabnzbd/sabnzbd.ini | grep 'password' | awk -F [=' '] NR==2'{print "password " $4}' | awk -v q='"' '$1=","q$1q":",$2=q$2q')"
[ "$sabnzbdpass" = ","'"password"'"":" "'""""'"" ] && sabnzbdpass=
}

transmissionstuff() {
transuser="$(sudo docker exec transmission-openvpn bash -c 'echo "$TRANSMISSION_RPC_USERNAME"')"
transpass="$(sudo docker exec transmission-openvpn bash -c 'echo "$TRANSMISSION_RPC_PASSWORD"')"
}

#colors
BLACK="$(tput setaf 0)"
RED="$(tput setaf 1)"
GREEN="$(tput setaf 2)"
BLUE="$(tput setaf 4)"
YELLOW="$(tput setaf 3)"
MAGENTA="$(tput setaf 5)"
CYAN="$(tput setaf 6)"
WHITE="$(tput setaf 7)"
BLUEBG="$(tput setab 4)"
WHITEBG="$(tput setab 7)"
NC="$(tput sgr0)"

updatecheck
#requirementcheck
